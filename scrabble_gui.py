"""
Scrabble Clone
"""
# TODO DYNAMIC RESIZING BASED ON SCREEN_HEIGHT/WIDTH in on_resize
from tkinter.tix import MAIN
from turtle import update
import arcade
import arcade.gui
from matplotlib.pyplot import pie
from numpy import tile
import numpy
from scrabble_classes import TileType
import scrabble_classes as Scrabble
from scrabble_classes import Scrabble as scrabble

from aenum import Enum, NoAlias

# Screen title and size
SCREEN_TITLE = "Scrabble"
ROW, COL = 15, 15
WIDTH, HEIGHT = 45, 45
BAG_WIDTH, RACK_HEIGHT = 90, 50
MARGIN = 5
SCREEN_WIDTH = (WIDTH + MARGIN) * COL + MARGIN + BAG_WIDTH + 200
SCREEN_HEIGHT = (HEIGHT + MARGIN) * ROW + MARGIN
DEFAULT_LINE_HEIGHT = 45
DEFAULT_FONT_SIZE = 20


class GUI_STATE(Enum):
    _settings_ = NoAlias
    MAIN = 1
    GAME = 2
    RULES = 3
    END = 4

# Used to calculate placement of the play button when in game


def align_play_button_y() -> int:
    """
    Calculate y coordinate of play button in GAME state
    """
    return -1 * (SCREEN_HEIGHT / 2) + 45


def align_play_button_x() -> int:
    """
    Calculate x coordinate of play button in GAME state
    """
    return (SCREEN_WIDTH / 2) - 70

# Returns the index of the tile a piece is placed upon


def determine_tile(x, y) -> list[int]:
    """
    Calculate the grid position based on a coordinate
    """
    # some math stuff idk, ask Dylan if you want an explanation
    return [abs(ROW - 1 - (y // (HEIGHT + MARGIN))), (x // (WIDTH + MARGIN))]

# inverse of determine_tile


def determine_coords(coords: list[int]) -> list[int]:
    #######
    # Y IS 0 X IS 1
    #######
    r = [0, 0]
    # edge case for 0 in x or y
    if coords[0] == 0:
        # center for first
        r[0] = 725
    if coords[1] == 0:
        # center for first
        r[1] = 28
    # else calculate center of tile

    if coords[0] != 0:
        r[0] = SCREEN_HEIGHT - (coords[0] * (HEIGHT + MARGIN)) - 28

    if coords[1] != 0:
        r[1] = ((WIDTH + MARGIN) // 2) + ((WIDTH + MARGIN) * coords[1]) + 2
    return r


class GUI(arcade.Window):
    """ Main application class. """

    def __init__(self, game: Scrabble):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE, resizable=True)

        arcade.set_background_color(arcade.color.AIR_FORCE_BLUE)
        # GUI STATE
        self.gui_state = GUI_STATE.MAIN
        # Initialize Scrabble Game State
        self.Game = game
        # Track the position of the tile stuck to the cursor
        self.held_tile = None

        # Keep track of where it came from in-case it needs to be returned
        self.held_tile_origin = None
        self.held_tile_coords = None

        # Main Screen Manager
        self.manager = arcade.gui.UIManager()
        self.manager.enable()
        # Vertical BoxGroup for aligning buttons
        self.v_box = arcade.gui.UIBoxLayout()
        # Create buttons
        start_button = arcade.gui.UIFlatButton(text="Play", width=200)
        rules_button = arcade.gui.UIFlatButton(text="Rules", width=200)
        quit_button = arcade.gui.UIFlatButton(text="Quit", width=200)
        # Add buttons to BoxGroup
        self.v_box.add(start_button.with_space_around(bottom=20))
        self.v_box.add(rules_button.with_space_around(bottom=20))
        self.v_box.add(quit_button)
        # Handle Button events
        start_button.on_click = self.on_click_start
        rules_button.on_click = self.on_click_rules
        quit_button.on_click = self.on_click_quit
        # Create widget that holds v_box (centers the buttons)
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.v_box
            )
        )

        # create sprite list
        self.bag = arcade.SpriteList()
        self.piece_mounts = arcade.SpriteList()

        # build exchange
        self.exchange = arcade.SpriteList()
        self.exchange_box = arcade.SpriteSolidColor(
            75, 75, arcade.color.NEON_GREEN)
        self.exchange_box.center_x = SCREEN_WIDTH - 175
        self.exchange_box.center_y = SCREEN_HEIGHT - 200
        self.exchange.append(arcade.create_text_sprite(
            "Swap", SCREEN_WIDTH - 210, SCREEN_HEIGHT - 150, arcade.color.WHITE, font_size=24))
        self.exchange.append(self.exchange_box)

        self.played_tiles = arcade.SpriteList()
        self.scores = arcade.SpriteList()

        self.scores.append(arcade.create_text_sprite(
            f"CPU: {self.Game.cpu_score}", SCREEN_WIDTH - 275, SCREEN_HEIGHT - 550, arcade.color.WHITE, font_size=24))
        self.scores.append(arcade.create_text_sprite(
            f"Player: {self.Game.player_score}", SCREEN_WIDTH - 275, SCREEN_HEIGHT - 600, arcade.color.WHITE, font_size=24))
        self.scores.append(arcade.create_text_sprite(
            f"Pieces Remaining: {len(self.Game.piece_bag)}", SCREEN_WIDTH - 275, SCREEN_HEIGHT - 650, arcade.color.WHITE, font_size=24))

        # self.curr_pieces is used to keep track of what pieces are in the current play
        self.curr_pieces = []
        # exchange pieces are pieces added to be exchanged
        self.exchange_pieces_list = []
        # Store the board grid for drawing purposes (1D-Array Representation)
        self.grid_sprite_list = arcade.SpriteList()
        # 2D-Array representation
        self.grid_sprites = []

        # now draw the board
        for row in range(len(self.Game.board.board)):
            self.grid_sprites.append([])
            for col in range(len(self.Game.board.board[0])):
                x = col * (WIDTH + MARGIN) + (WIDTH / 2 + MARGIN)
                y = row * (HEIGHT + MARGIN) + \
                    (HEIGHT / 2 + MARGIN)
                # logic to color tiles based on type

                # for center
                if self.Game.board.get_tile([row, col]).type == TileType.START:
                    sprite = arcade.SpriteSolidColor(
                        WIDTH, HEIGHT, arcade.color.RED)

                # for double_word
                elif self.Game.board.get_tile([row, col]).type == TileType.DOUBLE_WORD:
                    sprite = arcade.SpriteSolidColor(
                        WIDTH, HEIGHT, arcade.color.GRANNY_SMITH_APPLE)

                # for triple_word
                elif self.Game.board.get_tile([row, col]).type == TileType.TRIPLE_WORD:
                    sprite = arcade.SpriteSolidColor(
                        WIDTH, HEIGHT, arcade.color.GOLD)

                # for double_letter
                elif self.Game.board.get_tile([row, col]).type == TileType.DOUBLE_LETTER:
                    sprite = arcade.SpriteSolidColor(
                        WIDTH, HEIGHT, arcade.color.BLUE)

                # for tripple letter
                elif self.Game.board.get_tile([row, col]).type == TileType.TRIPLE_LETTER:
                    sprite = arcade.SpriteSolidColor(
                        WIDTH, HEIGHT, arcade.color.PURPLE)
                else:  # color almond
                    sprite = arcade.SpriteSolidColor(
                        WIDTH, HEIGHT, arcade.color.ALMOND)

                sprite.center_x = x
                sprite.center_y = y
                self.grid_sprite_list.append(sprite)
                self.grid_sprites[row].append(sprite)

        # Game Screen Manager
        # create play button
        # manager for play button
        self.play_button_manager = arcade.gui.UIManager()
        self.play_button_manager.enable()
        # Create box layout to hold button
        self.play_button_box = arcade.gui.UIBoxLayout()
        # create play button and add it to box, also set it's on_click
        play_button = arcade.gui.UIFlatButton(text="Play", width=100)
        play_button.on_click = self.on_click_play
        self.play_button_box.add(play_button)
        self.play_button_manager.add(
            arcade.gui.UIAnchorWidget(
                align_x=align_play_button_x(),
                align_y=align_play_button_y(),
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.play_button_box
            )
        )

        # Rules Screen Manager
        self.text_objects_rules = []
        self.text_start_x = 0
        self.text_start_y = SCREEN_HEIGHT - DEFAULT_LINE_HEIGHT * 1.5
        self.title = arcade.Text(
            text="Rules of the Game",
            start_x=self.text_start_x,
            start_y=self.text_start_y,
            color=arcade.color.ALLOY_ORANGE,
            font_size=DEFAULT_FONT_SIZE * 2,
            width=SCREEN_WIDTH,
            align="center"
        )
        self.line1 = arcade.Text(
            text="Tiles: ",
            start_x=self.text_start_x,
            start_y=self.text_start_y - 50,
            color=arcade.color.ALLOY_ORANGE,
            font_size=DEFAULT_FONT_SIZE * 1.5,
            width=SCREEN_WIDTH,
            align="center"
        )
        self.line2 = arcade.Text(
            text="Green = Double Word",
            start_x=self.text_start_x,
            start_y=self.text_start_y - 100,
            color=arcade.color.GRANNY_SMITH_APPLE,
            font_size=DEFAULT_FONT_SIZE * 1.5,
            width=SCREEN_WIDTH,
            align="center"
        )
        self.line3 = arcade.Text(
            text="Gold = Triple Word",
            start_x=self.text_start_x,
            start_y=self.text_start_y - 150,
            color=arcade.color.GOLD,
            font_size=DEFAULT_FONT_SIZE * 1.5,
            width=SCREEN_WIDTH,
            align="center"
        )
        self.line4 = arcade.Text(
            text="Blue = Double Letter",
            start_x=self.text_start_x,
            start_y=self.text_start_y - 200,
            color=arcade.color.BLUE,
            font_size=DEFAULT_FONT_SIZE * 1.5,
            width=SCREEN_WIDTH,
            align="center"
        )
        self.line5 = arcade.Text(
            text="Purple = Double Word",
            start_x=self.text_start_x,
            start_y=self.text_start_y - 250,
            color=arcade.color.PURPLE,
            font_size=DEFAULT_FONT_SIZE * 1.5,
            width=SCREEN_WIDTH,
            align="center"
        )
        self.text_objects_rules.append(self.title)
        self.text_objects_rules.append(self.line1)
        self.text_objects_rules.append(self.line2)
        self.text_objects_rules.append(self.line3)
        self.text_objects_rules.append(self.line4)
        self.text_objects_rules.append(self.line5)
        # End Screen Manager
        self.end_manager = arcade.gui.UIManager()
        self.end_screen_built = False

        # Background for UI
        self.background_texture = arcade.load_texture(
            file_name="background_texture.jpg")

    def update_scores(self):
        self.scores = arcade.SpriteList()
        self.scores.append(arcade.create_text_sprite(
            f"CPU: {self.Game.cpu_score}", SCREEN_WIDTH - 275, SCREEN_HEIGHT - 550, arcade.color.WHITE, font_size=24))
        self.scores.append(arcade.create_text_sprite(
            f"Player: {self.Game.player_score}", SCREEN_WIDTH - 275, SCREEN_HEIGHT - 600, arcade.color.WHITE, font_size=24))
        self.scores.append(arcade.create_text_sprite(
            f"Pieces Remaining: {len(self.Game.piece_bag)}", SCREEN_WIDTH - 275, SCREEN_HEIGHT - 650, arcade.color.WHITE, font_size=24))

    '''
    Method to update the sprites in the players bag, used after the play
    '''

    def update_bag_sprites(self):
        self.offset = 0
        self.bag = arcade.SpriteList()
        for piece in self.Game.player_pieces:  # Add player pieces to side view

            # create sprite to act as piece mounts, these are kept seperately from bag
            tile_spot = arcade.SpriteSolidColor(
                WIDTH, HEIGHT, arcade.color.PURPLE)
            tile_spot.set_position(
                SCREEN_WIDTH - 45, SCREEN_HEIGHT - self.offset - 50)
            self.piece_mounts.append(tile_spot)

            # create tile
            new_tile = Draggable(piece)
            new_tile.set_position(
                SCREEN_WIDTH - 45, SCREEN_HEIGHT - self.offset - 50)
            self.offset += 65
            new_tile.flip()

            # make tile grabbable
            new_tile.grabbable = True
            self.bag.append(new_tile)

    def setup(self):
        """ Set up the game here. Call this function to restart the game. """
        self.update_bag_sprites()

    def on_draw(self):
        """ Render the screen. """
        # Clear the screen
        self.clear()
        # Draw background_texture
        arcade.draw_lrwh_rectangle_textured(bottom_left_x=0, bottom_left_y=0,
                                            width=SCREEN_WIDTH, height=SCREEN_HEIGHT,
                                            texture=self.background_texture)
        if self.gui_state == GUI_STATE.MAIN:
            # Clear background stuff, remove played tiles
            self.bag.clear()
            self.play_button_manager.disable()
            # remove played tiles
            for i in self.Game.curr_play:
                self.Game.remove_piece(i.coords)

            # clear play
            self.Game.curr_play = []
            self.Game.curr_played_pieces = []
            # enable manager buttons
            self.manager.enable()
            self.manager.draw()

        if self.gui_state == GUI_STATE.GAME:
            # disable end manager buttons
            self.end_manager.disable()
            # disable manager buttons
            self.manager.disable()
            # enable play button
            self.play_button_manager.enable()
            # Draw Board
            self.grid_sprite_list.draw()
            # draw exchange
            self.exchange.draw()
            # draw piece mounts
            self.piece_mounts.draw()
            # Draws tile stack on the right of the screen
            self.bag.draw()
            # draw computer played
            self.played_tiles.draw()
            # draw play button
            self.play_button_manager.draw()
            # draw score
            self.scores.draw()

            if self.Game.game_over:
                self.gui_state = GUI_STATE.END

        if self.gui_state == GUI_STATE.RULES:
            # Run the draw_rules routine
            self.draw_rules()

        if self.gui_state == GUI_STATE.END:
            self.manager.disable()
            self.play_button_manager.disable()
            # Create the end screen if it doesn't already exist
            if not self.end_screen_built:
                self.build_end_screen()
            else:
                self.end_manager.enable()
                self.end_manager.draw()

    def draw_rules(self):
        for text_obj in self.text_objects_rules:
            text_obj.draw()
            # Detect if text_obj is a list?? Or maybe look at UIManager to make a layout for rule panels

    def build_end_screen(self):
        """
        Generate End Screen with correct scores and snarky message
        """
        self.end_box = arcade.gui.UIBoxLayout(size_hint_min=250)
        ui_text_label = arcade.gui.UITextArea(text="Better Luck Next Time",
                                              width=450,
                                              height=40,
                                              font_size=18,
                                              font_name="Kenney Future")
        self.end_box.add(ui_text_label.with_space_around(bottom=20))

        self.ui_cpu_score = arcade.gui.UITextArea(text=f"CPU Score: {self.Game.cpu_score}",
                                                  width=250,
                                                  height=40,
                                                  font_size=14,
                                                  font_name="Kenny Future")

        self.ui_player_score = arcade.gui.UITextArea(text=f"Player Score: {self.Game.player_score}",
                                                     width=250,
                                                     height=40,
                                                     font_size=14,
                                                     font_name="Kenny Future")
        self.end_box.add(self.ui_cpu_score)
        self.end_box.add(self.ui_player_score)

        exit_button = arcade.gui.UIFlatButton(text="Exit", width=250)
        exit_button.on_click = self.on_click_quit
        self.end_box.add(exit_button)

        self.end_manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.end_box
            )
        )
        self.end_screen_built = True

    def on_mouse_press(self, x: float, y: float, button: int, modifiers: int):
        """ Called when the user presses a mouse button. """

        for i in self.curr_pieces:
            print(type(i))

        # print(len(self.Game.player_pieces))
        # print(len(self.Game.piece_bag))
        #print(x, y)

        tiles = arcade.get_sprites_at_point((x, y), self.bag)
        if len(tiles) > 0:
            if tiles[-1].grabbable and tiles[-1].piece.locked == False:
                self.held_tile = tiles[-1]
                self.held_tile_origin = self.held_tile.position
                self.pull_to_top(self.held_tile)
                if self.held_tile.hidden:
                    self.held_tile.flip()

        # if you are holding a piece: if the piece is in exchange pieces, remove it.
        if self.held_tile != None:
            if self.held_tile.piece in self.exchange_pieces_list:
                self.exchange_pieces_list.remove(self.held_tile.piece)

        print(self.exchange_pieces_list)
        # NOTE: This returns [y, x] since that is the format the board uses
        tile_coords = determine_tile(x, y)
        if tile_coords[0] < 15 and tile_coords[1] < 15 and self.Game.board.board[tile_coords[0], tile_coords[1]].piece != None:
            if self.Game.board.board[tile_coords[0], tile_coords[1]].piece.locked == False:
                # save piece coords
                self.held_tile_coords = [tile_coords[0], tile_coords[1]]
                print(f"origin_coords: {self.held_tile_coords}")
                self.Game.remove_piece([tile_coords[0], tile_coords[1]])

    def on_mouse_release(self, x: float, y: float, button: int,
                         modifiers: int):
        """ Called when the user presses a mouse button. """
        # only go if a tile is held
        if self.held_tile != None:
            # placed is set to true once a target has been found
            placed = False

            # determine tile being played on if piece is placed on the board
            tile_coords = determine_tile(x, y)
            # print(tile_coords)

            # First check to see if piece is being exchanged
            collide_list = arcade.get_sprites_at_point(
                (x, y), self.exchange)
            # print(collide_list)
            # Logic below handles snapping pieces
            # first check if piece is on any tile
            collide_list = arcade.get_sprites_at_point(
                (x, y), self.grid_sprite_list)

           # print(collide_list)
            if len(collide_list) > 0 and not placed:
                # Grab the relevant tile
                collided_tile = collide_list[-1]

                self.held_tile.center_x = collided_tile.center_x
                self.held_tile.center_y = collided_tile.center_y

                # play piece in the game
                self.Game.play_piece(self.held_tile.piece, [
                                     tile_coords[0], tile_coords[1]])
                # append to curr_pieces if not already in
                if self.held_tile not in self.curr_pieces:
                    self.curr_pieces.append(self.held_tile)

                placed = True

            else:
                # now check mounts
                collide_list = arcade.get_sprites_at_point(
                    (x, y), self.piece_mounts)

                # if a mount is found:
                if len(collide_list) > 0 and not placed:
                    # grab closest mount
                    collided_mount = collide_list[-1]
                    # center piece on mount
                    self.held_tile.center_x = collided_mount.center_x
                    self.held_tile.center_y = collided_mount.center_y

                    # remove from curr_pieces
                    if self.held_tile in self.curr_pieces:
                        self.curr_pieces.remove(self.held_tile)

                    placed = True

                # check to see if pieces are being recycled
                collide_list = arcade.get_sprites_at_point(
                    (x, y), self.exchange)

                if len(collide_list) > 0 and not placed:
                    # add piece to recycling list, and move it there
                    self.held_tile.center_x = collide_list[-1].center_x
                    self.held_tile.center_y = collide_list[-1].center_y

                    self.exchange_pieces_list.append(self.held_tile.piece)

                    placed = True

                if not placed:
                    print("bad placement")
                    # else bad placement, return tile
                    self.held_tile.center_x = self.held_tile_origin[0]
                    self.held_tile.center_y = self.held_tile_origin[1]
                    # replace piece
                    if self.held_tile_coords != None:
                        self.Game.play_piece(self.held_tile.piece, [
                                             self.held_tile_coords[0], self.held_tile_coords[1]])

                if self.held_tile_coords != None:
                    print(self.held_tile.piece.letter,
                          self.held_tile_coords[0], self.held_tile_coords[1])

        self.Game.board.print_board()

        self.held_tile = None

        #print(f'curr_play: {[tile.piece.letter for tile in self.Game.curr_play]}')

    def on_mouse_motion(self, x: float, y: float, dx: float, dy: float):
        """ User moves mouse """
        if self.held_tile != None:
            self.held_tile.center_x += dx
            self.held_tile.center_y += dy

    def on_key_press(self, symbol: int, modifiers: int):
        """ User presses key """
        if symbol == arcade.key.ESCAPE:

            self.gui_state = GUI_STATE.MAIN

    def on_key_release(self, symbol: int, modifiers: int):
        """ User releases key """
        pass

    def on_resize(self, width, height):
        """ This method is automatically called when the window is resized. """

        # Call the parent. Failing to do this will mess up the coordinates,
        # and default to 0,0 at the center and the edges being -1 to 1.
        super().on_resize(width, height)

        #print(f"Window resized to: {width}, {height}")
        # print(
        #    f"align_x: {align_play_button_x()}, align_y: {align_play_button_y()}")

    def pull_to_top(self, draggable: arcade.Sprite):
        """ Pull Tile to the top of rendering order """

        # Take the tile off the list
        self.bag.remove(draggable)

        # Attach to the back of the list
        self.bag.append(draggable)

    def on_click_start(self, event):
        """
        Start the game by updating the sprites and setting state to GAME
        """
        self.update_bag_sprites()
        self.gui_state = GUI_STATE.GAME

    def on_click_rules(self, event):
        """
        Show Rules page using RULES state
        """
        self.gui_state = GUI_STATE.RULES

    def on_click_quit(self, event):
        """
        Exit game and close window
        """
        arcade.exit()

    def on_click_play(self, event):
        """
        Play button in game view. Used to uh, make a play
        """
        print(len(self.exchange_pieces_list))
        # first verify that there is play
        if len(self.Game.curr_play) > 0 and len(self.exchange_pieces_list) == 0:
            # verify a valid play is made
            if self.Game.make_play():
                print("valid")

                # freeze played tiles
                for piece in self.curr_pieces:
                    piece.grabbable = False
                    print(piece.letter)
                    # remove piece from bag, append to played_tiles.
                    self.bag.remove(piece)
                    self.played_tiles.append(piece)
                # reset curr_pieces
                self.curr_pieces = []

                # have computer go, save played tiles in computer_played_tiles
                computer_played_tiles = self.Game.make_computer_move()
                self.Game.board.print_board()

                # Make board update with comp move
                for tile in computer_played_tiles:
                    # create piece using information from played tiles
                    coords = determine_coords(tile.coords)
                    new_tile = Draggable(tile.piece)
                    new_tile.set_position(coords[1], coords[0])
                    new_tile.flip()
                    self.played_tiles.append(new_tile)
                # refresh bag sprites and screen

        # if there is items in the exchange box, it is not the first turn, and curr_pieces is empty, exchange tiles and have player go
        if len(self.exchange_pieces_list) > 0 and not self.Game._first_turn and len(self.curr_pieces) == 0:

            self.Game.exchange_pieces(
                self.exchange_pieces_list, self.Game.player_pieces)
            self.exchange_pieces_list = []
            computer_played_tiles = self.Game.make_computer_move()
            self.Game.board.print_board()

            # Make board update with comp move
            for tile in computer_played_tiles:
                # create piece using information from played tiles
                coords = determine_coords(tile.coords)
                new_tile = Draggable(tile.piece)
                new_tile.set_position(coords[1], coords[0])
                new_tile.flip()
                self.played_tiles.append(new_tile)

        # clear lists used to keep track of placements
        self.exchange_pieces_list = []
        self.curr_pieces = []

        self.update_scores()  # This will also update curr_pieces
        self.update_bag_sprites()
        self.gui_state = GUI_STATE.GAME


class Draggable(arcade.Sprite):
    """ Tile sprite """

    def __init__(self, piece, scale=0.80):
        """ Card constructor """

        # Attributes for letter and pts of Tile
        self.letter = piece.letter
        self.pts = piece.pts
        self.piece = piece
        self.hidden = True
        self.grabbable = False
        # Image for Tile
        self.image_file_name = f"letters/{self.letter}.jpg"
        self.blank_file_name = "letters/None.jpg"

        # Call the parent (~built in sprite rendering and handling~)
        super().__init__(self.blank_file_name, scale, hit_box_algorithm="None")

    def flip(self):
        """
        Used to hide a tile
        Cosmetic
        """
        # Toggle Hidden
        self.hidden ^= True
        if self.hidden:
            self.texture = arcade.load_texture(self.blank_file_name)
        else:
            self.texture = arcade.load_texture(self.image_file_name)
