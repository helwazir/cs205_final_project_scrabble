'''
Main Integration for Scrabble game

'''

#Import dependencies
from scrabble_classes import Turn
import scrabble_classes as scrabble
from scrabble_gui import GUI
import arcade
from scrabble_classes import Turn

def main():
    #create scrabble object
    scrab = scrabble.Scrabble()

    #create gui
    window = GUI(scrab)
    window.setup()
    arcade.run()


if __name__ == "__main__":
   main()
