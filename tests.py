'''
Test cases for scrabble classes
'''
from types import NoneType
from scrabble_classes import Turn, Word
import scrabble_classes
import pickle
def test_scrabble_classes():
    scrab = scrabble_classes.Scrabble()
    #Check that validates first play

    scrab.player_pieces = [scrabble_classes.Piece(x) for x in 'helloelp']

    for x in scrab.player_pieces:
        print(x.letter)
    
    scrab.play_piece(scrab.player_pieces[0], [1, 2])
    scrab.play_piece(scrab.player_pieces[1], [1, 3])
    scrab.play_piece(scrab.player_pieces[2], [1, 4])
    scrab.play_piece(scrab.player_pieces[3], [1, 5])
    scrab.play_piece(scrab.player_pieces[4], [1, 6])
    
    if scrab.board.get_tile([1, 2]).piece.letter != "h":
        raise Exception("Test Case 1 Failed")
    if scrab.board.get_tile([1, 3]).piece.letter != "e":
        raise Exception("Test Case 2 Failed")
    if scrab.board.get_tile([1, 4]).piece.letter != "l":
        raise Exception("Test Case 3 Failed")
    if scrab.board.get_tile([1, 5]).piece.letter != "l":
        raise Exception("Test Case 4 Failed")
    if scrab.board.get_tile([1, 6]).piece.letter != "o":
        raise Exception("Test Case 5 Failed")
    
    #make play
    print("make_play1")
    scrab.make_play()

    #ensure reset
    if type(scrab.board.get_tile([1, 2]).piece) != NoneType:
        raise Exception("Test Case 6 Failed")

    scrab.player_pieces = [scrabble_classes.Piece(x) for x in 'helloelp']

    #now verify a valid play   
    scrab.play_piece(scrab.player_pieces[0], [7, 7])
    scrab.play_piece(scrab.player_pieces[1], [7, 8])
    scrab.play_piece(scrab.player_pieces[2], [7, 9])
    scrab.play_piece(scrab.player_pieces[3], [7, 10])
    scrab.play_piece(scrab.player_pieces[4], [7, 11])
    
    print("make_play2")
    scrab.make_play()

    #reestablish the players words
    scrab.player_pieces = [scrabble_classes.Piece(x) for x in 'helloelp']

    #make sure pieces remain
    if scrab.board.get_tile([7, 7]).piece.letter != "h":
        raise Exception("TestCase 7 Failed")
    if scrab.board.get_tile([7, 8]).piece.letter != "e":
        raise Exception("Test Case 8 Failed")
    if scrab.board.get_tile([7, 9]).piece.letter != "l":
        raise Exception("Test Case 9 Failed")
    if scrab.board.get_tile([7, 10]).piece.letter != "l":
        raise Exception("Test Case 10 Failed")
    if scrab.board.get_tile([7, 11]).piece.letter != "o":
        raise Exception("Test Case 11 Failed")

    #now play off of first play and verify that play is valid
    scrab.play_piece(scrab.player_pieces[5], [8, 7])
    scrab.play_piece(scrab.player_pieces[6], [9, 7])
    scrab.play_piece(scrab.player_pieces[7], [10, 7])

    scrab.make_play()
    #verify
    if scrab.board.get_tile([7, 7]).piece.letter != "h":
        raise Exception("Test Case 12 Failed")
    if scrab.board.get_tile([8, 7]).piece.letter != "e":
        raise Exception("Test Case 13 Failed")
    if scrab.board.get_tile([9, 7]).piece.letter != "l":
        raise Exception("Test Case 14 Failed")
    if scrab.board.get_tile([10, 7]).piece.letter != "p":
        raise Exception("Test Case 15 Failed")


    #verify tiles were removed
    if scrab.board.get_tile([10, 7]).piece.letter != 'p':
        raise Exception("Test Case 19 Failed")
    if scrab.board.get_tile([10, 8]).piece != None:
        raise Exception("Test Case 20 Failed")
    if scrab.board.get_tile([10, 9]).piece != None:
        raise Exception("Test Case 21 Failed")

    #verify turn works
    if scrab.turn != Turn.CPU:
        raise Exception("Test Case 22 Failed")

    
    

    #verify make_computer play works
    #ccheck CPU Score
    curr_score = scrab.cpu_score
    curr_pieces = scrab.cpu_pieces
    print(curr_score)
    print(scrab.cpu_score)
    if scrab.cpu_score != curr_score:
        raise Exception("Test Case 26 Failed")
    
    if scrab.cpu_pieces != curr_pieces:
        raise Exception("Test Case 26.5 Failed")

    print(scrab.cpu_score)
    
    scrab.board.print_board()
    #new board
    scrab = scrabble_classes.Scrabble()

    #test scores
    #[5, 8-11]
    scrab.player_pieces = [scrabble_classes.Piece(x) for x in 'roomyqw']
    scrab.play_piece(scrab.player_pieces[0], [5, 8])
    scrab.play_piece(scrab.player_pieces[1], [5, 9])
    scrab.play_piece(scrab.player_pieces[2], [5, 10])
    scrab.play_piece(scrab.player_pieces[3], [5, 11])

    test_word = Word([scrab.board.get_tile([5, 8]),
                    scrab.board.get_tile([5, 9]),
                    scrab.board.get_tile([5, 10]),
                   scrab.board.get_tile([5, 11])])

    if test_word.calc_score() != 8:
        raise Exception("Test Case 27 Failed")


    scrab = scrabble_classes.Scrabble()
    #testing case where playing through already played tiles
    scrab.player_pieces = [scrabble_classes.Piece(x) for x in 'seeepqo']
    scrab.play_piece(scrab.player_pieces[0], [7, 7])
    scrab.play_piece(scrab.player_pieces[1], [7, 8])
    scrab.play_piece(scrab.player_pieces[2], [7, 9])
    
    scrab.make_play()

    scrab.player_pieces = [scrabble_classes.Piece(x) for x in 'meffpqo']
    scrab.play_piece(scrab.player_pieces[0], [6, 8])
    scrab.play_piece(scrab.player_pieces[1], [7, 8])
    scrab.play_piece(scrab.player_pieces[2], [8, 8])
    scrab.play_piece(scrab.player_pieces[3], [9, 8])

    test_word = Word([scrab.board.get_tile([6, 8]),
                    scrab.board.get_tile([7, 8]),
                    scrab.board.get_tile([8, 8]),
                   scrab.board.get_tile([9, 8])])

    scrab.board.print_board()
    if test_word.calc_score() != 19:
        print(test_word.calc_score())
        raise Exception("Test Case 28 Failed")

    return True


def main():
    if test_scrabble_classes() == True:
        print("Test cases for classes passed")

main()
