from typing import List, Callable, Dict
import random
from aenum import Enum, NoAlias
import numpy as np

# Import scrabble word dictionary
scrabble_words_file = open("Collins Scrabble Words (2019).txt", "r")
# Parse the words from the file into a unique set, all lower case, separated by a newline character
scrabble_dict = set(scrabble_words_file.read().lower().split("\n"))

# Map letters to a score
letter_scores = {'a': 1, 'e': 1, 'i': 1, 'o': 1, 'u': 1, 'l': 1, 'n': 1, 's': 1, 't': 1, 'r': 1,
                 'd': 2, 'g': 2, 'b': 3, 'c': 3, 'm': 3, 'p': 3, 'f': 4, 'h': 4, 'v': 4, 'w': 4,
                 'y': 4, 'k': 5, 'j': 8, 'x': 8, 'q': 10, 'z': 10, '': 0}
vowels = ["a", "e", "i", "o", "u"]
letters_to_primes = {'a': 2, 'b': 3, 'c': 5, 'd': 7, 'e': 11, 'f': 13, 'g': 17, 'h': 19, 'i': 23, 'j': 29,
                     'k': 31, 'l': 37, 'm': 41, 'n': 43, 'o': 47, 'p': 53, 'q': 59, 'r': 61, 's': 67, 't': 71, 'u': 73,
                     'v': 79, 'w': 83, 'x': 89, 'y': 97, 'z': 101}


class TileType(Enum):
    _settings_ = NoAlias
    NORMAL = 1
    START = 1
    DOUBLE_WORD = 2
    TRIPLE_WORD = 3
    DOUBLE_LETTER = 2
    TRIPLE_LETTER = 3


class Direction(Enum):
    DOWN = [1, 0]
    RIGHT = [0, 1]


class Turn(Enum):
    PLAYER = 0
    CPU = 1


################################### CLASSES ###################################

class Piece:
    """
    A class used to represent a Scrabble piece

    ...

    Attributes
    ----------
    letter : str
        The letter of the piece
    score : int
        the point value of the piece
    """

    def __init__(self, letter: str = None) -> None:
        """
        Parameters
        ----------
        letter : str
            The letter of the piece
        locked: bool
            Whether or not the piece can be moved
        """
        self.locked = False
        self.letter = letter
        if letter is None:
            self.pts = 0
        else:
            self.pts = letter_scores[letter]


class Tile:
    """
    A class used to represent a tile (or square) on a Scrabble game board

    ...

    Attributes
    ----------
    coords : List[int]
        the [y, x] coordinates of the tile on the game board where the top left of the board is [0, 0] and the bottom right is [14, 14]
    x: int
        the x coordinate of the tile on the game board
    y: int
        the y coordinate of the tile on the game board
    piece: Piece
        the scrabble piece currently placed on the tile
    type: TileType
        the type of space the tile is (i.e. normal, double word, triple letter etc.)

    l_thld: int
        how many empty spaces there are to the left of the tile
    r_thld: int
        how many empty spaces there are to the right of the tile
    u_thld: int
        how many empty spaces there are above the tile
    d_thld: int
        how many empty spaces there are below the tile
    """

    def __init__(self, coords: List[int], type: TileType) -> None:
        """
        Parameters
        ----------
        coords : List[int]
            the [y, x] coordinates of the tile on the game board where the top left of the board is [0, 0] and the bottom right is [14, 14]
        type: TileType
            the type of space the tile is (i.e. normal, double word, triple letter etc.)
        """

        self.coords = coords
        self.x = coords[1]
        self.y = coords[0]
        self.piece: Piece = None
        self.type = type
        self.l_thld = 0
        self.r_thld = 0
        self.u_thld = 0
        self.d_thld = 0
        self.horizontal_playable: bool = True
        self.vertical_playable: bool = True


class Word:
    """
    A class used to represent a word in a Scrabble game. The word is build out of tiles with pieces on them

    ...

    Attributes
    ----------
    tiles : List[Tile]
        the collection of tiles with pieces that make up the word
    word : str
        the word spelt out by the pieces on the tiles
    direction: Direction
        the direction that the word is spelt in on the game board
    score: int
        the point value of the word
    """

    def __init__(self, tiles: List[Tile]) -> None:
        """
        Parameters
        ----------
        tiles : List[Tile]
            the collection of tiles with pieces that make up the word
        """

        self.tiles = tiles
        self.neighbors: List[Word] = []
        self.word: str = self.build_word()
        self.score = self.calc_score()

    def build_word(self) -> str:
        """ builds the word spelt out as a string 

        Returns
        -------
        str
            the word spelled out by the pieces on the tiles
        """

        word_str = ""
        for tile in self.tiles:
            word_str += tile.piece.letter
        return word_str

    def calc_score(self) -> int:
        """ 
        enumerates over all of the tiles making up the word and increments the score based on the values of the pieces on 
        said tiles, accounting for the type of tile the piece is on.

        Returns
        -------
        int
            the score of the word
        """
        score = 0
        word_mult = 1
        for tile in self.tiles:

            if tile.type is not None and tile.type is not TileType.START and tile.type is not TileType.NORMAL:
                if tile.type == TileType.DOUBLE_WORD:
                    word_mult *= 2
                    score += tile.piece.pts
                elif tile.type == TileType.TRIPLE_WORD:
                    word_mult *= 3
                    score += tile.piece.pts
                elif tile.type == TileType.DOUBLE_LETTER:
                    score += tile.piece.pts * 2
                elif tile.type == TileType.TRIPLE_LETTER:
                    score += tile.piece.pts * 3
            else:
                score += tile.piece.pts

        score *= word_mult

        return score


class Board:
    """
    a class used to represent a word in the game board of Scrabble

    ...

    Attributes
    ----------
    board : List[List[Tile]]
        a two dimensional list of tiles 
    """

    def __init__(self) -> None:
        self.board = self._build_board()

    def _build_board(self) -> np.ndarray:
        """ builds and populates the game board

        Returns
        -------
        List[List[Tile]]
            a two dimensional list of tiles (i.e. the game board)
        """

        board: List[List[Tile]] = []

        for i in range(15):

            row = [Tile([i, j], None) for j in range(15)]
            board.append(row)

        for i in range(1, 5):   # designate double letter spaces
            board[i][i].type = TileType.DOUBLE_WORD
            board[i][14 - i].type = TileType.DOUBLE_WORD
            board[14 - i][i].type = TileType.DOUBLE_WORD
            board[14 - i][14 - i].type = TileType.DOUBLE_WORD

        for i in range(0, 15, 7):   # designate triple word spaces
            board[i][0].type = TileType.TRIPLE_WORD
            board[i][7].type = TileType.TRIPLE_WORD
            board[i][14].type = TileType.TRIPLE_WORD

        for i in range(1, 15, 4):   # designate triple letter spaces
            board[5][i].type = TileType.TRIPLE_LETTER
            board[9][i].type = TileType.TRIPLE_LETTER
            board[i][5].type = TileType.TRIPLE_LETTER
            board[i][9].type = TileType.TRIPLE_LETTER

        for i, j in zip([3, 6, 7, 8, 11], [0, 2, 3, 2, 0]):  # designate double letter spaces
            board[i][j].type = TileType.DOUBLE_LETTER
            board[j][i].type = TileType.DOUBLE_LETTER
            board[i][14 - j].type = TileType.DOUBLE_LETTER
            board[14 - j][i].type = TileType.DOUBLE_LETTER
        board[6][6].type = TileType.DOUBLE_LETTER
        board[8][8].type = TileType.DOUBLE_LETTER
        board[6][8].type = TileType.DOUBLE_LETTER
        board[8][6].type = TileType.DOUBLE_LETTER

        board[7][7].type = TileType.START   # designate starting tile

        return np.array(board)

    def print_board(self) -> None:
        for row in self.board:
            for tile in row:
                if tile.piece is None:
                    print(f' - ', end='')
                else:
                    print(f' {tile.piece.letter} ', end='')
            print('')
        print('')

    def up(self, tile: Tile) -> Tile:
        """ returns the tile directly above the given tile

        Parameters
        ----------
        tile: Tile
            the tile whose upper neighbor is to be returned

        Returns
        -------
        Tile
            the tile directly above the given tile
        """

        if tile.y <= 14 and tile.y > 0 and tile.x <= 14 and tile.x >= 0:
            return self.board[tile.y - 1][tile.x]

    def down(self, tile: Tile) -> Tile:
        if tile.y < 14 and tile.y >= 0 and tile.x <= 14 and tile.x >= 0:
            return self.board[tile.y + 1][tile.x]

    def left(self, tile: Tile) -> Tile:
        if tile.y <= 14 and tile.y >= 0 and tile.x <= 14 and tile.x > 0:
            return self.board[tile.y][tile.x - 1]

    def right(self, tile: Tile) -> Tile:
        if tile.y <= 14 and tile.y >= 0 and tile.x < 14 and tile.x >= 0:
            return self.board[tile.y][tile.x + 1]

    def get_tile(self, coords: List[int]) -> Tile:
        """ returns the tile on the board at the given coordinates

        Parameters
        ----------
        coords: List[int]
            the coordinates of the tile to be retrieved

        Returns
        -------
        Tile
            the tile at the given coordinates on the board
        """

        if coords[0] <= 14 and coords[0] >= 0 and coords[1] <= 14 and coords[1] >= 0:
            return self.board[coords[0]][coords[1]]

    def place_piece(self, piece: Piece, coords: List[int]) -> None:
        """ places a given piece on the tile at the given coordinates

        Parameters
        ----------
        piece: Piece
            the piece to place on the board
        coords: List[int]
            the coordinates of the tile on which to place the piece
        """

        self.get_tile(coords).piece = piece


class Scrabble():
    """
    A class used to represent the board game Scrabble and appropriate actions necessary to play the game

    ...

    Attributes
    ----------
    turn: Turn
        the current player whose turn it is
    board: Board
        the Board object representing the game board
    piece_bag : List[Piece]
        the collection of pieces from which players draw from
    curr_played_pieces: List[Piece]
        the collection of pieces being played during the current turn
    curr_play: List[Tile]
        the collection of tiles being played on during the current turn
    player_pieces: List[Piece]
        the collection of pieces the player has in their hand
    cpu_pieces: List[Piece]
        the collection of pieces the computer has in their hand
    player_words: List[str]
        a list of all the words previously played by the player
    cpu_words: List[str]
        a list of all the words previously played by the computer
    player_score: int
        the player's score
    cpu_score: int
        the computer's score
    first_turn: bool
        True if it is the first turn of the game, otherwise False
    game_over: bool
        True if the game has ended, otherwise False
    """

    def __init__(self) -> None:
        self.turn = Turn.PLAYER
        self.board = Board()
        self.piece_bag = self._create_piece_bag()
        self.curr_played_pieces: List[Piece] = []
        self.curr_play: List[Tile] = []
        self.player_pieces = [self._draw_new_piece() for i in range(7)]
        self.cpu_pieces = [self._draw_new_piece() for i in range(7)]
        self.player_words: List[Word] = []
        self.cpu_words: List[Word] = []
        self.player_score: int = 0
        self.cpu_score: int = 0
        self._first_turn: bool = True
        self.playable_tiles: List[Tile] = []
        self.word_lookup_dict = self._build_word_lookup_dict()
        self.game_over = False

    def _create_piece_bag(self) -> List[Piece]:
        """ creates, populates, and randomizes a list with the appropriate amount of each piece necessary for the game

        Returns
        -------
        List[Piece]
            a randomized list of all the pieces to be used in the game
        """

        piece_bag = []
        for i in range(12):
            if i < 12:
                piece_bag.append(Piece('e'))
            if i < 9:
                piece_bag.extend([Piece('a'), Piece('i')])
            if i < 8:
                piece_bag.append(Piece('o'))
            if i < 6:
                piece_bag.extend([Piece('n'), Piece('r'), Piece('t')])
            if i < 4:
                piece_bag.extend(
                    [Piece('d'), Piece('s'), Piece('u'), Piece('l')])
            if i < 3:
                piece_bag.append(Piece('g'))
            if i < 2:
                piece_bag.extend([Piece('b'), Piece('c'), Piece('f'), Piece('h'), Piece('m'),
                                  Piece('p'), Piece('v'), Piece('w'), Piece('y')])  # removed blank piece
            if i < 1:
                piece_bag.extend(
                    [Piece('j'), Piece('k'), Piece('q'), Piece('x'), Piece('z')])
        random.shuffle(piece_bag)
        return piece_bag

    def _draw_new_piece(self) -> Piece:
        """ picks and removes the first piece in piece_bag 
        Returns
        -------
        Piece
            the piece "drawn" from piece_bag if there is a piece to draw, otherwise return None
        """

        if len(self.piece_bag) > 0:
            return self.piece_bag.pop(0)
        else:
            print('Bag is empty!')
            self.game_over = True
            return None

    def exchange_pieces(self, pieces_to_exchange: List[Piece], hand: List[Piece]) -> None:
        """ 
        Removes pieces from the user's collection of pieces, adds them back to the piece bag, shuffles the piece bag, 
        and replaces the removed pieces from the user's collection with new pieces from the piece bag

        Parameters
        ----------
        pieces_to_exchange: Piece
            the list of pieces the user wishes to exchange for new ones
        """

        for piece in pieces_to_exchange:
            hand.remove(piece)
            # self.player_pieces.remove(piece)     # remove piece from player's collection of pieces
            # add removed piece back to piece bag
            self.piece_bag.append(piece)

        random.shuffle(self.piece_bag)  # shuffle bag

        for i in range(len(pieces_to_exchange)):
            hand.append(self._draw_new_piece())

    def play_piece(self, piece: Piece, coords: List[int]) -> bool:
        """ 
        places a piece on the board at the given coordinates, adds the played piece to the list of the pieces
        being played in the current turn (curr_played_pieces) and adds the tile on which the piece was played
        to the list of tiles being playe on in the current turn (curr_play)

        Parameters
        ----------
        piece: Piece
            the piece being played

        coords: List[int]
            the coordinates of the tile to place the piece on
        """

        # COMMENT THIS OUT FOR TESTING
        if piece not in self.player_pieces:
            return False

        if self.board.get_tile(coords).piece is not None:
            return False

        # self.board.place_piece(piece, coords)
        self.board.board[coords[0]][coords[1]].piece = piece
        tile = self.board.get_tile(coords)

        self.curr_played_pieces.append(piece)
        self.curr_play.append(self.board.get_tile(coords))
        return True

    def remove_piece(self, coords: List[int]) -> None:
        """ 
        sets the pieces field of the tile at the given coordinates to None

        Parameters
        ----------
        coords: List[int]
            the coordinates of the tile whose piece is to be removed
        """

        print(self.curr_played_pieces, self.curr_play)
        y, x = coords
        tile = self.board.board[y][x]
        self.curr_played_pieces.remove(tile.piece)
        self.curr_play.remove(tile)
        tile.piece = None

        return tile.piece

    def _score_words(self, words: List[Word]) -> int:
        """ calculates the sum of the scores of the words in the given list

        Returns
        -------
        Piece
            the piece "drawn" from piece_bag if there is a piece to draw, otherwise return None
        """

        score = 0
        for word in words:
            score += word.score
        return score

    def _update_playable(self, words: List[Word]) -> None:
        """ updates the list of playable tiles with changes caused by new words created in words

        Parameters
        ----------
        words: List[Tile]
            the list of words whose tiles' playability needs to be determined and added to or removed from playable_tiles
        """
        for word in words:
            for tile in word.tiles:
                # print(type(tile))
                if tile.u_thld == 0 and tile.d_thld == 0:
                    tile.vertical_playable = False
                if tile.l_thld == 0 and tile.r_thld == 0:
                    tile.horizontal_playable = False

                thlds = [tile.l_thld, tile.r_thld, tile.u_thld, tile.d_thld]
                if tile in self.playable_tiles and all(thld == 0 for thld in thlds):
                    self.playable_tiles.remove(tile)
                elif tile not in self.playable_tiles and any(thld != 0 for thld in thlds):
                    self.playable_tiles.append(tile)

        for tile in self.playable_tiles:    # update horizontal_playable and vertical_playable fields for each playable tile
            if tile.u_thld == 0 and tile.d_thld == 0:
                tile.vertical_playable = False
            if tile.l_thld == 0 and tile.r_thld == 0:
                tile.horizontal_playable = False
            if not tile.horizontal_playable and not tile.vertical_playable:
                self.playable_tiles.remove(tile)

    def _update_space_threshold(self, tile: Tile, dir_func: Callable[[Tile], Tile], n: int) -> tuple[int, Tile]:
        """ calculates the new space threshold of a tile and finds the first non-empty tile

        Parameters
        ----------
        tile: Tile
            the tile the begin finding the threshold from

        Returns
        -------
        tuple[int, Tile]
            a tuple with the space between the given tile and the terminating tile, and the terminating tile
        """
        thld = 0
        curr_tile = tile
        next_tile = dir_func(self.board, curr_tile)

        for i in range(n):
            if next_tile is not None:
                if next_tile.piece is None:
                    thld += 1
                    curr_tile = next_tile
                    next_tile = dir_func(self.board, curr_tile)
                else:
                    return thld, next_tile
            else:
                break
        return thld, curr_tile

    def _update_all_space_thresholds(self, tiles: List[Tile]) -> None:
        """ updates all of the space thresholds of each tile in a list of tiles

        Parameters
        ----------
        tiles: List[Tile]
            the list of tiles whose space thresholds are to be updated
        """
        for tile in tiles:
            thld, term_tile = self._update_space_threshold(
                tile, Board.up, min(7, tile.y))
            tile.u_thld = thld
            term_tile.d_thld = thld

            thld, term_tile = self._update_space_threshold(
                tile, Board.down, min(7, 14 - tile.y))
            tile.d_thld = thld
            term_tile.u_thld = thld

            thld, term_tile = self._update_space_threshold(
                tile, Board.left, min(7, tile.x))
            tile.l_thld = thld
            term_tile.r_thld = thld

            thld, term_tile = self._update_space_threshold(
                tile, Board.right, min(7, 14 - tile.x))
            tile.r_thld = thld
            term_tile.l_thld = thld

    def _get_direction(self, tiles: List[Tile]) -> Direction:
        """ calculates the direction that the given tiles are laid in on the game board

        Parameters
        ----------
        tiles: List[Tile]
            the list of tiles whose direction is to be found

        Returns
        -------
        Direction
            the direction that the tiles are laid in on the game board
        """

        if len(tiles) == 0:
            return None
        if tiles[0].x - tiles[1].x < 0 and tiles[0].y - tiles[1].y == 0:
            return Direction.RIGHT
        elif tiles[0].y - tiles[1].y < 0 and tiles[0].x - tiles[1].x == 0:
            return Direction.DOWN
        return None  # Might need to default to an actual direction if this breaks stuff down the line

    def _dirmost_tile(self, tile: Tile, dir_func: Callable[[Tile], Tile]) -> Tile:
        """ recursively finds the terminating tile in a direction specified by the function get_dir_func

        Parameters
        ----------
        tile: Tile
            the tile to start from
        get_dir_func: Callable[[], Tile]
            the function returning the adjacent tile in a direction, to be called recursively

        Returns
        -------
        Tile
            terminating tile in a given direction in a straight line starting from, and including, the starting tile
        """

        curr_tile = tile

        next_tile = dir_func(self.board, curr_tile)
        if next_tile is None:
            return curr_tile
        elif next_tile.x < 0 or next_tile.x > 14 or next_tile.y < 0 or next_tile.y > 14:
            return curr_tile
        elif next_tile.piece is None:
            return curr_tile

        else:
            return self._dirmost_tile(next_tile, dir_func)

    def _find_word(self, tile: Tile, direction: Direction) -> Word:
        """ finds and, if a word is found, builds the word made with the given tile in the given direction

        Parameters
        ----------
        tile: Tile
            the tile to start finding a word from
        direction: Direction
            the direction to find the word in

        Returns
        -------
        Word
            the found word, if no word was found then None
        """

        word_length = 0

        # Find the terminating tiles to the right and left of the tile
        if direction == Direction.RIGHT:
            head = self._dirmost_tile(tile, Board.left)
            tail = self._dirmost_tile(tile, Board.right)
            word_length = tail.x - head.x

        # Find the terminating tiles above and below the tile
        elif direction == Direction.DOWN:
            head = self._dirmost_tile(tile, Board.up)
            tail = self._dirmost_tile(tile, Board.down)
            word_length = tail.y - head.y

        if word_length > 0:
            start_y, start_x = head.coords
            end_y, end_x = tail.coords
            brd = self.board.board
            # Create new word object
            new_word_tiles = np.ndarray.flatten(
                self.board.board[start_y:end_y + 1, start_x:end_x + 1])
            new_word = Word(new_word_tiles)
            return new_word

        return None

    def _find_all_words(self, tiles: List[Tile]) -> tuple[bool, List[Word]]:
        """ finds all of the new words created by the current play and determines whether all found words are valid

        Parameters
        ----------
        tiles: List[Tile]
            the tiles to find new words from

        Returns
        -------
        tuple(bool, List[Word]):
            returns True and the list of new words if all words found are in the dictionary, otherwise returns False, None
        """

        words = []

        # If only one tile was played we need to check both directions (might be a sleeker way to do this tbh, kinda clunky rn)
        if len(tiles) == 1:
            for direction in Direction:
                word = self._find_word(tiles[0], direction)
                if word is not None:
                    if not word.word in scrabble_dict:
                        return False, None
                    words.append(word)
            if len(words) == 0:
                return False, None
            # print(words)
            return True, words

        # Find main word played
        main_word_direction = self._get_direction(tiles)
        main_word = self._find_word(tiles[0], main_word_direction)
        if main_word is None:
            return False, None

        if not any(tile in self.playable_tiles for tile in main_word.tiles) and not self._first_turn:
            return False, None

        if self._first_turn:  # if it is the first turn of the game, make sure that a tile is played on the starting space
            if not any(tile.coords == [7, 7] for tile in tiles):
                return False, None
            self._first_turn = False

        # if played tiles are not all in the main word (i.e. tiles are not connected in a line)
        if not all(tile in main_word.tiles for tile in tiles):
            return False, None
        # if word is not a real word
        if not main_word.word in scrabble_dict:
            # if not main_word.word in spell:
            return False, None

        words.append(main_word)

        # get opposite direction of main word
        aux_word_dir = Direction(
            [abs(x - y) for x, y in zip(main_word_direction.value, [1, 1])])
        # Find all auxiliary words
        for tile in tiles:
            aux_word = self._find_word(tile, aux_word_dir)
            if aux_word is not None:
                if not aux_word.word in scrabble_dict:   # SHOULD THIS BE aux_word.word ????
                    return False, None
                words.append(aux_word)

        return True, words

    def _remove_and_draw_pieces(self, hand: List[Piece], played_pieces: List[Piece], turn: Turn) -> None:
        """ removes the played pieces from the specified hand and draws new pieces from the piece bag to replace them

        Parameters
        ----------
        hand: List[Piece]
            the hand to remove pieces from and draw into

        played_pieces: List[Piece]
            the list of played pieces to remove from the hand
        """
        for piece in played_pieces:
            if piece in hand:
                hand.remove(piece)

        for i in range(len(played_pieces)):
            drawn_piece = self._draw_new_piece()
            if drawn_piece is None:
                break
            else:
                hand.append(drawn_piece)

    def make_play(self) -> bool:
        """ 
        validates the current play being attempted by the user and applies the play to the board if it is valid. updates
        the necesarry fields appropriately at the end of the turn

        Returns
        -------
        bool:
            True if the current play was valid and applied to the board, otherwise False
        """
        brd = self.board.board
        valid, words = self._find_all_words(self.curr_play)

        if not valid:
            self._remove_pieces(self.curr_play)
            self.curr_play = []
            self.curr_played_pieces = []
            return False

        for tile in self.curr_play:
            tile.type = TileType.NORMAL
        self._remove_and_draw_pieces(
            self.player_pieces, self.curr_played_pieces, Turn.PLAYER)
        self.player_score += self._score_words(words)
        self.player_words.extend(words)  # add words to player words
        self._update_all_space_thresholds(self.curr_play)
        self._update_playable(words)
        for piece in self.curr_played_pieces:
            piece.locked = True
        self.turn = Turn.CPU
        self.curr_play = []
        self.curr_played_pieces = []

        # update first turn
        self._first_turn = False
        return True

    def _build_word_lookup_dict(self) -> Dict[int, List[str]]:
        """ 
            Using a mapping f(x_n) = p_n, where x_n is the nth letter of the alphabet and p_n is the nth prime number, we encode
        each letter as a prime number. We then calculate the prime product of a word by taking the product over the prime encoding
        of each letter in the word. By the fundamental theorem of arithmetic we have that any two words containing the same letters
        will have the same prime product, this prime product is unique to words containing these letters. We then create a dictionary
        mapping prime products to the list of words that share said prime product. This allows us to quickly find what words can be
        created given a collection of letters. To do this we calculate the prime product of the letters we want to use to make a word
        and index into the dictionary to get the list of words we can make. (note that even if our collection of letters is not orderd
        such that it is a valid word, it still shares the prime product of any valid words using the same letters since multiplication
        is commutitave). This function creates the prime_product:list_of_words dictionary and returns it.

        Returns
        -------
        Dict[int: List[str]]:
            the word lookup dictionary
        """
        scrabble_words = np.loadtxt(
            'Collins Scrabble Words (2019).txt', dtype=str)
        scrabble_words = np.char.lower(scrabble_words)

        encoded_words_dict = {}

        for word_str in scrabble_words:
            encoded_word = 1
            for letter in word_str:
                encoded_word *= letters_to_primes[letter]
            if encoded_word in encoded_words_dict.keys():
                encoded_words_dict[encoded_word].append(word_str)
            else:
                encoded_words_dict[encoded_word] = [word_str]

        return encoded_words_dict

    def _encode_pieces(self, pieces: List[Piece]) -> int:
        """ encodes each piece in a list to a prime number based on its letter and takes the product over all pieces

        Parameters
        ----------
        pieces: List[Piece]
            the list of pieces to calculate the prime product of

        Returns
        -------
        int:
            the resulting prime product of the list of pieces
        """

        encoded_pieces = 1
        for piece in pieces:
            encoded_pieces *= letters_to_primes[piece.letter]
        return encoded_pieces

    # code to find combinations lightly massaged from https://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/

    def _find_valid_combinations(self, board_letter: str, n: int, r: int) -> List[List[Piece]]:
        data = [None]*r
        combinations = []
        self._combination_recursive(
            combinations, board_letter, data, 0, n - 1, 0, r)
        return combinations

    def _combination_recursive(self, combinations, board_letter: str, data, start, end, index, r):
        if (index == r):
            comb = data[0:r]
            if any(piece.letter in vowels for piece in comb) or board_letter in vowels:
                combinations.append(comb)
            return

        i = start
        while(i <= end and end - i + 1 >= r - index):
            self.cpu_pieces
            data[index] = self.cpu_pieces[i]
            self._combination_recursive(
                combinations, board_letter, data, i + 1, end, index + 1, r)
            i += 1

    def _remove_pieces(self, tiles: List[Tile]) -> None:
        """ 
        sets the piece field of every tile in the given list to None

        Parameters
        ----------
        tiles: List[Tile]
            the list of tiles to 'remove' pieces from
        """

        for tile in tiles:
            tile.piece = None

    def _cpu_place_pieces(self, pieces: List[Piece], tiles: List[Tile]) -> None:
        """ 
        places the pieces from the pieces list 'onto' the tiles in the tiles list such that tiles[i].piece = pieces[i]

        Parameters
        ----------
        pieces: List[Piece]
            the pieces to place on the tiles

        tiles: List[Tile]
            the tiles to place the pieces on
        """

        for piece, tile in zip(pieces, tiles):
            tile.piece = piece

    def _tiles_and_pieces_to_play(self, word_tiles: List[Tile], word: str, board_tile: Tile) -> tuple[List[Piece], List[Tile]]:
        """ 
        finds the correspondence between the pieces being played, the tiles being played on, and the word being played. 
        i.e. the function figures out which piece to place on which tile in order to create the word such that the word 
        fits on the board

        Parameters
        ----------
        word_tiles: List[Tile]
            the list of tiles that are to be encompassed by the new word

        word: str
            the new word being played

        board_tile: Tile
            the tile that the word is being played off of

        Returns
        -------
        tuple[List[Piece], List[Tile]]
            a tuple containing a list of pieces and a list of tiles such that, to play the current word, we must set played_tiles[i].piece = played_pieces[i]
        """

        played_tiles = []
        played_pieces = []
        piece_letter_indicies = [piece.letter for piece in self.cpu_pieces]
        for tile, letter in zip(word_tiles, word):
            if tile is not board_tile and tile.piece is None:
                piece_idx = piece_letter_indicies.index(letter)
                played_tiles.append(tile)
                played_pieces.append(self.cpu_pieces[piece_idx])

        return played_pieces, played_tiles

    def make_computer_move(self) -> list[Tile]:
        """ 
        Finds all possible words that the computer can create given the pieces in its hand and the pieces on the board
        that can be played off of. It enumerates through all of the possible words and finds and makes the play that will 
        maximize the score of the words made.

        Returns
        -------
        List[Tile]:
            the list of played tiles, used to update the GUI

        """

        max_comb = None
        max_word = ""
        max_score = 0
        max_tile = None
        valid = False

        for board_tile in self.playable_tiles:  # loop through all tiles on the board that can be played off of
            board_piece = board_tile.piece
            board_letter = board_piece.letter

            # the number of empty spaces in each direction from the current board_tile
            l = board_tile.l_thld
            r = board_tile.r_thld
            u = board_tile.u_thld
            d = board_tile.d_thld

            # the maximum number of tiles we can play off of the current tile
            max_len = min(7, max(l + r, u + d))

            for i in range(1, max_len + 1):  # find possible words of size i
                # get list of possible combinations of size i of pieces from self.cpu_pieces
                combs = self._find_valid_combinations(board_letter, 7, i)
                for comb in combs:
                    # add board_piece to current combination so that it is included when calculating the prime product of the pieces being considered
                    comb.append(board_piece)
                    # calculate the prime product of the pieces currently being considered
                    encoded_comb = self._encode_pieces(comb)

                    if encoded_comb in self.word_lookup_dict.keys():   # if the prime product is associated with any words
                        # try playing each word
                        for decoded_word in self.word_lookup_dict[encoded_comb]:

                            # check if playing the word is a valid play and what the score would be
                            valid, _, _, curr_score = self._cpu_play(
                                comb, decoded_word, board_tile, play=False)
                            if curr_score > max_score and valid:    # if this is a better move than any other we have encountered then set the play values to the values of this play
                                max_comb = comb
                                max_word = decoded_word
                                max_score = curr_score
                                max_tile = board_tile

        if max_word == '':  # there is no valid play for the computer to make
            print('Computer has no valid play to make, exchanging tiles...')
            self.exchange_pieces(self.cpu_pieces, self.cpu_pieces)
            return

        else:
            valid, played_tiles, words, score = self._cpu_play(
                max_comb, max_word, max_tile, play=True)   # make the best play found
            # update all the necesarry fields at the end of a turn
            for tile in played_tiles:
                tile.type = TileType.NORMAL
            for tile in played_tiles:
                tile.piece.locked = True
            self._remove_and_draw_pieces(self.cpu_pieces, max_comb, Turn.CPU)
            self.cpu_score += score
            self.cpu_words.extend(words)
            self._update_all_space_thresholds(played_tiles)
            self._update_playable(words)
            self.turn = Turn.PLAYER
            return played_tiles

    # TODO: remove comb as an argument???

    def _cpu_play(self, comb: List[Piece], played_word: str, board_tile: Tile, play: bool) -> tuple[bool, List[Tile], List[Word], int]:
        """ 
        attempts a play for the computer player and determines if said play is valid, what tiles the new word encompasses, 
        the new words created, and the score resulting from the play

        Parameters
        ----------
        comb: List[Piece]
            the current combination of pieces being considered for the move

        played_word: str
            the current word being attempted to play

        board_tile: Tile
            the tile that the word is being played off of

        play: bool
            whether or not to actually place tiles to make the play or just check if the play is valid and then remove the tiles

        Returns
        -------
        tuple[bool, List[Tile], List[Word], int]
            a tuple containing whether the play is valid, a list of the tiles contained in the new word, a list of the new words created, and the score of the play
        """

        words = []
        word_tiles = []
        valid = False
        play_score = 0

        l = board_tile.l_thld
        r = board_tile.r_thld
        u = board_tile.u_thld
        d = board_tile.d_thld
        y = board_tile.y
        x = board_tile.x
        board_letter = board_tile.piece.letter
        size = len(played_word) - 1

        indicies = [i for i, letter in enumerate(
            played_word) if letter == board_letter]
        for idx in indicies:
            if board_tile.horizontal_playable and size - idx <= r and idx <= l + r:
                word_tiles = np.ndarray.flatten(
                    self.board.board[y:y + 1, x - idx:x + (size + 1 - idx)])
                valid = True
                break

            elif board_tile.vertical_playable and size - idx <= d and idx <= u + d:
                word_tiles = np.ndarray.flatten(
                    self.board.board[y - idx:y + (size + 1 - idx), x:x + 1])
                valid = True
                break

        # if not valid:   # if word_tiles has not been created, means the word cannot be played on the curren tile
        #     return valid, None, None, 0

        curr_cpu_pieces, curr_cpu_tiles = self._tiles_and_pieces_to_play(
            word_tiles, played_word, board_tile)
        self._cpu_place_pieces(curr_cpu_pieces, curr_cpu_tiles)

        if len(curr_cpu_tiles) > 0:
            valid, words = self._find_all_words(curr_cpu_tiles)
        if words:
            play_score = self._score_words(words)

        if not play or not valid:
            self._remove_pieces(curr_cpu_tiles)

        return valid, word_tiles, words, play_score

    def test_computer_move(self):
        # test_letters = ['g', 'e', 'f', 't', 'd', 'i', 'a']
        # test_letters = ['a', 'y', 'd', 'w', 'd', 'm', 'n']
        # self.cpu_pieces = [Piece(letter) for letter in test_letters]
        print(f'CPU Words: {[word.word for word in self.cpu_words]}')
        while not self.game_over:
            tic = time.perf_counter()
            self.make_computer_move()
            toc = time.perf_counter()
            self.board.print_board()


def main():
    def print_board() -> None:
        for row in scrab.board.board:
            for tile in row:
                if tile.piece is None:
                    print(f' - ', end='')
                else:
                    print(f' {tile.piece.letter} ', end='')
            print('')
        print('')

    def print_info(words: List[Word]) -> None:
        for word in words:
            print(f'WORD: {word.word}')
            for tile in word.tiles:
                print(f'Letter: {tile.piece.letter}, Coords: {tile.coords}')
                print(
                    f'l_thld: {tile.l_thld}, r_thld: {tile.r_thld}, u_thld: {tile.u_thld}, d_thld: {tile.d_thld}\n')

    def starting_test_state(dir: Direction, word: str, cpu_hand: str = '') -> None:
        if dir == Direction.RIGHT:
            for i, letter in enumerate(word):
                scrab.play_piece(Piece(letter), [7, 6 + i])
            print(f'Valid play: {scrab.make_play()}')

        else:
            for i, letter in enumerate(word):
                scrab.play_piece(Piece(letter), [6 + i, 7])
            print(f'Valid play: {scrab.make_play()}')

        if len(cpu_hand) != 0:
            scrab.cpu_pieces = [Piece(letter) for letter in cpu_hand]

    scrab = Scrabble()

    scrab.play_piece(Piece('h'), [7, 6])
    scrab.play_piece(Piece('e'), [7, 7])
    print(f'Valid play: {scrab.make_play()}')


if __name__ == "__main__":
    main()
